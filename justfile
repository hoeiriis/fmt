just:
    @just --list

check: (_check "defmt") (_check "log")

_check features:
    cargo check --features {{features}}

